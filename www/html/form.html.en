<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<!-- $Id: form.html.en,v 1.3 2006-02-10 14:07:24 sdalu Exp $ -->
 
<!--                                                                      -->
<!-- CONTACT     : http://www.dnsdoctor.org/                              -->
<!-- AUTHOR      : Stephane D'Alu <sdalu@sdalu.com>                       -->
<!--                                                                      -->
<!-- CREATED     : 2002/10/01 13:58:17                                    -->
<!-- REVISION    : $Revision: 1.3 $                                      -->
<!-- DATE        : $Date: 2006-02-10 14:07:24 $                           -->
<!--                                                                      -->
<!-- CONTRIBUTORS: (see also CREDITS file)                                -->
<!--                                                                      -->
<!--                                                                      -->
<!-- LICENSE     : GPL v2                                                 -->
<!-- COPYRIGHT   : Stephane D'Alu (c) 2004                                -->
<!--                                                                      -->
<!-- This file is part of DNSdoctor.                                      -->
<!--                                                                      -->
<!-- DNSdoctor is free software; you can redistribute it and/or modify it -->
<!-- under the terms of the GNU General Public License as published by    -->
<!-- the Free Software Foundation; either version 2 of the License, or    -->
<!-- (at your option) any later version.                                  -->
<!--                                                                      -->
<!-- DNSdoctor is distributed in the hope that it will be useful, but     -->
<!-- WITHOUT ANY WARRANTY; without even the implied warranty of           -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU    -->
<!-- General Public License for more details.                             -->
<!--                                                                      -->
<!-- You should have received a copy of the GNU General Public License    -->
<!-- along with DNSdoctor; if not, write to the Free Software Foundation, -->
<!-- Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA          -->
<!--                                                                      -->
<!--                                                                      -->

  <head>
    <link rel="stylesheet" href="@HTML_PATH@/style/dnsdoctor.css" type="text/css"/>
    <link rel="stylesheet" href="@HTML_PATH@/style/default.css"   type="text/css"/>
    <link rel="icon"       href="@HTML_PATH@/img/ddoc-fav.png"    type="image/png"/>

    <link rel="start" title="DNSdoctor main page"
	  href="@HTML_PATH@/" type="text/html"/>
    <link rel="help"	
	  href="#help" type="text/html"/>
    <link rel="alternate" title="DNSdoctor" 
	  href="@HTML_PATH@/fr/" type="text/html" hreflang="fr"/>
    <link rel="alternate" title="DNSdoctor" 
	  href="@HTML_PATH@/zh/" type="text/html" hreflang="zh"/>

    <script src="@HTML_PATH@/js/formvalidation.js" type="text/javascript"></script>
    <script type="text/javascript">
      ddoc_form_setlocale("You need to give at least a zone to test.");
    </script>

    <title>DNSdoctor</title>
  </head>

  <body>
    <!-- Lang -->
    <div class="lang">
      <a href="@HTML_PATH@/en/" id="current"><abbr title="English">en</abbr></a> |
      <a href="@HTML_PATH@/fr/"><abbr title="Français">fr</abbr></a> |
      <a href="@HTML_PATH@/zh/"><abbr title="简体中文">zh</abbr></a></div>


    <!-- Logo -->
    <h1>Diagnose your domain</h1>
    <div>
      <img class="ddoc-logo" src="@HTML_PATH@/img/logo_l.png" alt="DNSdcotor logo"/>
    </div>


    <!-- Propaganda -->
    <p><a href="http://www.dnsdoctor.org/">DNSdoctor</a> 
      will perform several tests on your domain to ensure that it is 
      correctly configured, so that you can enjoy a trouble free
      domain. For information about the potential risks of having
      a misconfigured domain, take a look at the
      <a href="http://www.dnsdoctor.org/">DNS doctor homepage</a>.</p>
      
    <!-- Warning about waiting time -->
    <blockquote class="ddoc-warning">
      <p><img src="@HTML_PATH@/img/notepad.png" alt="Note: "
              style="padding: 0.5ex; float: left;"/>
      The time required to completely diagnose a zone can take
      from 30 seconds up to <b>5 minutes</b> depending on the network
      speed of the server being accessed. 
      If it takes more than a minute it generally means we are
      encountering problems accessing your nameservers 
      (errors in your DNS configuration, firewall, ...) and are 
      waiting for timeout.</p>
    </blockquote>
    
    <!-- A little help -->
    <p>You will find in the <a href="#help">help</a> section, a description
      of the different options that you can use to customize this tool,
      but the default setting should be ideal for nearly all users; there 
      is also a short explanation on how to read the diagnostic.</p>

    <!-- Form -->
    <form id="ddocform" method="get" action="@HTML_PATH@/cgi-bin/diagnose.cgi"
          class="ddoc-form" onsubmit="return ddoc_form_validate(this)">

      <!-- Basic information -->
      <h2>Form</h2>
      <p><abbr title="Internationalized Domain Names">IDN</abbr> are not
      currently supported, if you want to use such names,
      you will need to enter the
      <abbr title="coding starting with: xn--">punnycode</abbr> form.</p>

      <fieldset class="ddoc-zoneinfo">
        <legend>Zone information</legend>
        <table>
	  <tr>
	    <td><img src="@HTML_PATH@/img/zone.png" alt="" title="Zone"/></td>
	    <td colspan="4">
	      <input name="zone" type="text" size="62" value=""/></td>
	  </tr>
          <tr><th></th><th>Nameserver</th><th>IP address(es)</th></tr>
	  <tr>
	    <td><img src="@HTML_PATH@/img/primary.png" alt="" title="Primary"/></td>
	    <td><input name="ns0"  type="text" size="24" value=""/></td>
	    <td><input id="ips0" name="ips0"
                       type="text" size="35" value=""/></td>
	  </tr>
	  <tr>
	    <td><img src="@HTML_PATH@/img/secondary.png" alt="" title="Secondary"/></td>
	    <td><input name="ns1"  type="text" size="24" value=""/></td>
	    <td><input name="ips1" type="text" size="35" value=""/></td>
	  </tr>
	  <tr>
	    <td><img src="@HTML_PATH@/img/secondary.png" alt="" title="Secondary"/></td>
	    <td><input name="ns2"  type="text" size="24" value=""/></td>
	    <td><input name="ips2" type="text" size="35" value=""/></td>
	  </tr>
	  <tr>
	    <td><img src="@HTML_PATH@/img/secondary.png" alt="" title="Secondary"/></td>
	    <td><input name="ns3"  type="text" size="24" value=""/></td>
	    <td><input name="ips3" type="text" size="35" value=""/></td>
	  </tr>
	  <tr>
	    <td><img src="@HTML_PATH@/img/secondary.png" alt="" title="Secondary"/></td>
	    <td><input name="ns4"  type="text" size="24" value=""/></td>
	    <td><input name="ips4" type="text" size="35" value=""/></td>
	  </tr>
	  <tr>
	    <td><img src="@HTML_PATH@/img/secondary.png" alt="" title="Secondary"/></td>
	    <td><input name="ns5"  type="text" size="24" value=""/></td>
	    <td><input name="ips5" type="text" size="35" value=""/></td>
	  </tr>
	  <tr>
	    <td><img src="@HTML_PATH@/img/secondary.png" alt="" title="Secondary"/></td>
	    <td><input name="ns6"  type="text" size="24" value=""/></td>
	    <td><input name="ips6" type="text" size="35" value=""/></td>
	  </tr>
	  <tr>
	    <td><img src="@HTML_PATH@/img/secondary.png" alt="" title="Secondary"/></td>
	    <td><input name="ns7"  type="text" size="24" value=""/></td>
	    <td><input name="ips7" type="text" size="35" value=""/></td>
	  </tr>
        </table>
      </fieldset>

      <!-- Buttons -->
      <div>
        <input type="submit" value=" Diagnose! "/>
        <script type="text/javascript">
	  document.writeln('<input type="button" value=" Clear " onclick="ddoc_form_clear(this.form)" />')
        </script>
      </div>

      <!-- Options -->
      <h2>Options</h2>
      <fieldset class="ddoc-options">
      <legend>Output</legend>
        <table>
          <tr>
	    <td><input type="checkbox" id="intro" name="intro"
                        checked="checked" value="t" />
	      <label for="intro">Zone summary</label></td>
	    <td><label for="format">Output:</label>
              <select id="format" name="format">
		<option value="html" selected="selected">HTML</option>
		<option value="text"                    >Text</option>
	      </select>
	    </td>
	    <td><label for="lang">Language:</label>
              <select id="lang" name="lang">
		<option value="en" selected="selected">English</option>
		<option value="fr"                    >Français</option>
		<option value="zh"                    >简体中文</option>
	      </select>
	    </td>
          </tr><tr>
	    <td><input type="checkbox" id="testname" name="testname"
                       value='t'/>
	      <label for="testname">Test name</label></td>
	    <td><input type="checkbox" name="explain" id="explain"
                       value='t' checked="checked"/>
	      <label for="explain">Explanations</label></td>
	    <td><input type="checkbox" name="details" id="details"
                       value='t' checked="checked"/>
	      <label for="details">Details</label></td>
          </tr><tr>
	    <td><input type="radio" id="progress1" name="progress"
                       value='counter' checked="checked"/>
	      <label for="progress1">Progress bar</label></td>
	    <td><input type="radio" id="progress2" name="progress"
                       value='testdesc'/>
	      <label for="progress2">Description</label></td>
	    <td><input type="radio" id="progress3" name="progress"
                       value=''/>
	      <label for="progress3">Nothing</label></td>
          </tr>
        </table>
      </fieldset>

      <fieldset class="ddoc-options">
      <legend>Error report</legend>
        <table>
	  <tr>
	    <td><input type="radio" id="errorlvl1" name="errorlvl"
                       value="" checked="checked"/>
	      <label for="errorlvl1">Default error</label></td>
	    <td><input type="radio" id="errorlvl2" name="errorlvl"
                       value="allfatal"/>
	      <label for="errorlvl2">All fatals</label></td>
	    <td><input type="radio" id="errorlvl3" name="errorlvl"
                       value="allwarning"/>
	      <label for="errorlvl3">All warnings</label></td>
	  </tr>
	  <tr>
	    <td><input type="checkbox" id="dontstop" name="dontstop"
                       value="nostop"/>
	      <label for="dontstop">Don't stop on fatal</label></td>
	    <td><input type="checkbox" id="fatalonly" name="fatalonly"
                       value="t"/>
	      <label for="fatalonly">Fatal only</label></td>
	    <td><input type="checkbox" id="reportok" name="reportok"
                       value="t"/>
	      <label for="reportok">Report ok</label></td> 
	  </tr>
	  <tr>
	    <td><label for="profile">Profile:</label>
              <select id="profile" name="profile">
		<option value="automatic" selected="selected">automatic</option>
                <optgroup label="automatic profiles">
		  <option value="default"           >Default</option>
		  <option value="reverse"           >Reverse</option>
		</optgroup>
                <optgroup label="available profiles">
		  <option value="afnic"             >AFNIC</option>
		</optgroup>
	      </select></td>
	    <td><label for="report">Sorted by:</label>
              <select id="report" name="report">
		<option value="byseverity"
                        selected="selected">severity</option>
		<option value="byhost"     >host</option>
	      </select>
	    </td>
	    <td></td>
	  </tr>
        </table>
      </fieldset>

      <fieldset class="ddoc-options">
      <legend>Extra tests performed</legend>
        <table>
	  <tr>
	    <td><input type="checkbox" id="chkmail" name="chkmail"
                       value='t' checked="checked"/>
	      <label for="chkmail">Mail delivery</label></td>
	    <td><input type="checkbox" id="chkzone" name="chkzone"
                       value='t' disabled="disabled"/>
	      <label for="chkzone">Zone transfer</label></td>
	    <td><input type="checkbox" id="chkrir" name="chkrir"
                       value='t' disabled="disabled"/>
	      <label for="chkrir"><acronym title="Regional Internet Registry">RIR</acronym> databases</label></td>
	  </tr>
        </table>
      </fieldset>

      <fieldset class="ddoc-options">
      <legend>Transport layer</legend>
        <table>
	  <tr>
	    <td><input type="checkbox" id="ipv4" name="transp3"
                       value="ipv4" checked="checked"/>
	      <label for="ipv4"><acronym title="Internet Protocol version 4">IPv4</acronym></label></td>
	    <td><input type="checkbox" id="ipv6" name="transp3"
                       value="ipv6" checked="checked"/>
	      <label for="ipv6"><acronym title="Internet Protocol version 6">IPv6</acronym></label></td>
	  </tr>
	  <tr>
	    <td><input type="radio" id="transp4std" name="transp4"
                       value="std" checked="checked"/>
	      <label for="transp4std"><acronym title="UDP with fallback on TCP for truncated answers">STD</acronym></label></td>
	    <td><input type="radio" id="transp4udp" name="transp4"
                       value="udp"/>
	      <label for="transp4udp"><acronym title="User Datagram Protocol">UDP</acronym></label></td>
	    <td><input type="radio" id="transp4tcp" name="transp4"
                       value="tcp"/>
	      <label for="transp4tcp"><acronym title="Transport Control Protocol">TCP</acronym></label></td>
	  </tr>
        </table>
      </fieldset>
    </form>
    

    <!-- Help -->
    <h2 id="help">Help</h2>
    <h3>Form</h3>
    <table rules="rows" class="ddoc-help">
	<tr><th colspan="2">Zone Information</th></tr>
	<tr>
	  <td>Zone</td>
	  <td>Name of the domain to be diagnosed.</td>
	</tr>
	<tr>
	  <td>Primary</td>
	  <td>Primary nameserver (the one in 
	    the SOA record if it is public).</td>
	</tr>
	<tr>
	  <td>Secondary</td>
	  <td>Secondary nameservers (all the NS records used for 
	    delegating the zone, except the one listed above).</td>
	</tr>
	<tr>
	  <td>IPs</td>
	  <td>List of IP addresses for the nameserver.
	    <ul>
	      <li>IP addresses are only required if they can't be resolved
		(ie: they are in the zone that you want to register).</li>
	      <li>if you have several addresses you must use a space or
		a comma as the separator.</li>
	      <li>the form accepts IPv4 and IPv6 addresses.</li>
	    </ul>
	  </td>
	</tr>

	<tr><th colspan="2">Output</th></tr>
	<tr>
	  <td>zone summary</td>
	  <td>Includes a summary about the zone and its nameservers in 
	    the generated report</td>
	</tr>
	<tr>
	  <td>test name</td>
	  <td>Includes the name of the test that has been performed
	    when reporting errors</td>
	</tr>
	<tr>
	  <td>explanations</td>
	  <td>Includes an explanation (when the test failed) about 
	    the purpose of the test and why you should fix it.</td>
	</tr>
	<tr>
	  <td>details</td>
	  <td>Includes details (when the test failed) about 
	    the culprit elements.</td>
	</tr>
	<tr>
	  <td>progress bar</td>
	  <td>Display information about the test progression 
	    using a a progress bar (<em>require javascript and
	    Mozilla or IE5+ for correct rendering</em>).</td>
	</tr>
	<tr>
	  <td>description</td>
	  <td>Give a short description of the test when it is performed.</td>
	</tr>
	<tr>
	  <td>nothing</td>
	  <td>Don't display information about the test progression.</td>
	</tr>
	<tr>
	  <td>report</td>
	  <td>Select the type of generated report you want.</td>
	</tr> 
	<tr>
	  <td>format</td>
	  <td>Select the format in which you want the report 
	    (HTML or plain text).</td>
	</tr> 
	<tr>
	  <td>language</td>
	  <td>Select the language that you want the report generated in.</td>
	</tr> 

	<tr><th colspan="2">Error report</th></tr>
	<tr>
	  <td>default error</td>
	  <td>Errors are reported with the default severity associated 
	    with them.</td>
	</tr>
	<tr>
	  <td>all fatals</td>
	  <td>All errors are considered fatals.</td>
	</tr>
	<tr>
	  <td>all warnings</td>
	  <td>All errors are considered warnings.</td>
	</tr>
	<tr>
	  <td>don't stop on fatal</td>
	  <td>Keep going even after encountering a fatal error
	    (<em>this could lead to some unexpected results</em>).</td>
	</tr>
	<tr>
	  <td>report ok</td>
	  <td>Report test that passed.</td>
	</tr>

	<tr><th colspan="2">Extra tests performed</th></tr>
	<tr>
	  <td>mail delivery</td>
	  <td>Perform extra checking on mail delivery for typical 
	    mail accounts (hostmaster, postmaster, ...) associated with
	    domain names.</td>
	</tr>
	<tr>
	  <td>zone transfer</td>
	  <td>Perform additional tests on the zone retrieved after
	    a <em>zone transfer</em>.</td>
	</tr>
	<tr>
	  <td>RIR databases</td>
	  <td>Check that IP addresses used are registered in the RIR
	    databases</td>
	</tr>

	<tr><th colspan="2">Transport layer</th></tr>
	<tr>
	  <td>IPv4, IPv6</td>
	  <td>Select the routing layer (if none are selected it will
	    default to IPv4 and IPv6).</td>
	</tr>
	<tr>
	  <td>STD, UDP, TCP</td>
	  <td>Select the transport layer you want for interrogating
	    your nameservers.</td>
	</tr>
    </table>

    <h3>Results</h3>
    <ul>
      <li>the word <i>generic</i> means that the error is eitheir
	unrelated or present on all nameservers,</li>
      <li>when an error is between [brackets], this means that the test
	failed for external reason, the reason is displayed next to it.</li>
    </ul>


    <!-- What / Who / When -->
    <div>
      <hr/>
      <span style="float: right;">
        <a href="http://jigsaw.w3.org/css-validator/check/referer">
	  <img style="border:0;width:88px;height:31px;"
	       src="http://jigsaw.w3.org/css-validator/images/vcss" 
  	       alt="Valid CSS!"/></a>
        <a href="http://validator.w3.org/check/referer">
	  <img style="border:0;width:88px;height:31px;"
	       src="http://www.w3.org/Icons/valid-xhtml11"
	       alt="Valid XHTML 1.1!"/></a>
      </span>
Release: $Name: not supported by cvs2svn $ <br/>
<!-- Created: Fri Sep 27 16:34:31 CEST 2002 -->
<!-- hhmts start -->
Last modified: Fri Feb 10 13:39:50 CET 2006
<!-- hhmts end -->
    </div>
  </body>

<!-- Local Variables: -->
<!-- mode: html       -->
<!-- End:             -->

</html>
