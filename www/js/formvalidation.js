// $Id: formvalidation.js,v 1.1.1.1 2004-08-11 18:51:04 sdalu Exp $

// 
// CONTACT     : http://www.dnsdoctor.org/
// AUTHOR      : Stephane D'Alu <sdalu@nic.fr>
//
// CREATED     : 2003/02/26 18:38:10
// REVISION    : $Revision: 1.1.1.1 $ 
// DATE        : $Date: 2004-08-11 18:51:04 $
//
// CONTRIBUTORS: (see also CREDITS file)
//
//
// LICENSE     : GPL v2
// COPYRIGHT   : AFNIC (c) 2003
//
// This file is part of DNSdoctor.
//
// DNSdoctor is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// DNSdoctor is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DNSdoctor; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

function ddoc_form_setlocale(emptyzone) {
  ddoc_form_l_emptyzone = emptyzone;
}

function ddoc_form_validate(form) {
  var zone = form["zone"].value;
  zone = zone.replace(/^\s+/,'').replace(/\s+$/,'');

  if (zone.length == 0) {
    alert(ddoc_form_l_emptyzone);
    return false;
  } else {
    return true;
  }
}

function ddoc_form_clear(form) {
  var i;

  if (form["zone"])
    form["zone"].value = "";
  for (i = 0 ; form["ns"+i] ; i++)
    form["ns" +i].value = "";
  for (i = 0 ; form["ips"+i] ; i++)
    form["ips" +i].value = "";

  return 0;
}
