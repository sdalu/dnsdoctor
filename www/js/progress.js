// $Id: progress.js,v 1.1.1.1 2004-08-11 18:51:04 sdalu Exp $

// 
// CONTACT     : http://www.dnsdoctor.org/
// AUTHOR      : Stephane D'Alu <sdalu@sdalu.com>
//
// CREATED     : 2002/10/02 13:58:17
// REVISION    : $Revision: 1.1.1.1 $ 
// DATE        : $Date: 2004-08-11 18:51:04 $
//
// CONTRIBUTORS: (see also CREDITS file)
//
//
// LICENSE     : GPL v2
// COPYRIGHT   : AFNIC (c) 2003
//
// This file is part of DNSdoctor.
//
// DNSdoctor is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// DNSdoctor is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DNSdoctor; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

/**
 * convert a time in sec into a string
 *  possible formats are 'mm:ss', 'hh:mm:ss' or '--:--'
 */
function ddoc_sec_to_timestr(sec) {
  if (sec < 0)
    return "--:--";

  hrs = Math.floor(sec / 3600); sec %= 3600;
  min = Math.floor(sec / 60);   sec %= 60;
  
  if (sec < 10)
    sec = "0" + sec;
   
  if (hrs > 0) {
    if (min < 10)
      min = "0" + min;
    return hrs + ":" + min + ":" + sec;
  } else {
    return min + ":" + sec;
  }
}

/**
 * convert a speed into a string (2 digits after the coma)
 */
function ddoc_speed_tostr(speed) {
  if (speed < 0)
    return "--.--";

  speed = speed * 100;
  cnt = Math.floor(speed) % 100;
  if (cnt < 10)
    cnt = "0" + cnt;
  unt = Math.floor(speed / 100);

  return unt + "." + cnt;
}

/**
 * switch off elements
 */
function ddoc_element_off(id) {
  document.getElementById(id).style.display = "none";
}

/**
 * remove id (so that it can be reused)
 */
function ddoc_element_clear_id(id) {
  document.getElementById(id).id = "";
}

/*======================================================================*/

/**
 * initialize locale for the progress bar (ie: internationalisation)
 */
function ddoc_pgr_setlocale(tprogress, progress, test, speed, time, na) {
  ddoc_pgr_l_title_progress = tprogress;
  ddoc_pgr_l_progress       = progress;
  ddoc_pgr_l_test           = test;
  ddoc_pgr_l_speed          = speed;
  ddoc_pgr_l_time           = time;
  ddoc_pgr_l_na             = na;
}

/**
 * quiet mode (no titles)
 */
function ddoc_pgr_setquiet(quiet) {
  ddoc_pgr_quiet = quiet;
}

/**
 * start progress bar
 */
function ddoc_pgr_start(count) {
  ddoc_pgr_starttime      = (new Date()).getTime();
  ddoc_pgr_processed      = 0;
  ddoc_pgr_last_processed = -1;
  ddoc_pgr_ticks          = 0;
  ddoc_pgr_totaltime      = 0;
  ddoc_pgr_totalsize      = count;
  ddoc_pgr_barsize        = 300;
  ddoc_pgr_tickersize	= 40;

  s  = "";
  if (! ddoc_pgr_quiet) {
    s += "<h2  id=\"ddoc-pgr-title\">" + ddoc_pgr_l_title_progress + "</h2>";
  }

  s += "<div id=\"ddoc-pgr-pbar\">";
  s += "<table id=\"ddoc-pgr-pbar-out\"><tr><td>";
  s += "<table id=\"ddoc-pgr-pbar-in\" style='border-collapse: collapse;'>";
  // titles
  s += "<tr>";
  s += "<td colspan=4>" + ddoc_pgr_l_progress + "</td>";
  s += "<td style='width: 2em;'></td>"
  s += "<td style='text-align: center;'>&nbsp;&nbsp;" + ddoc_pgr_l_test  + "&nbsp;&nbsp;</td>";
  s += "<td style='text-align: center;'>&nbsp;&nbsp;" + ddoc_pgr_l_speed + "&nbsp;&nbsp;</td>";
  s += "<td style='text-align: center;'>&nbsp;&nbsp;" + ddoc_pgr_l_time  + "&nbsp;&nbsp;</td>";
  s += "</tr>";
  // progress bar information
  s += "<tr>";
  s += "<td id=\"ddoc-pgr-pct\"   style='text-align: right; width: 4em'></td>";
  if (ddoc_pgr_totalsize <= 0) {
    // infinit
    s += "<td id=\"ddoc-pgr-pct0\"  style='border-style: solid none solid solid;'></td>";
    s += "<td id=\"ddoc-pgr-pct1\"  style='border-style: solid none; width: " + ddoc_pgr_tickersize + "px'></td>";
    s += "<td id=\"ddoc-pgr-pct2\"  style='border-style: solid solid solid none;'></td>";
  } else {
    // limited
    s += "<td id=\"ddoc-pgr-pct0\"  style='width: 0px'></td>";
    s += "<td id=\"ddoc-pgr-pct1\"  style='border-style: solid none solid solid;'></td>";
    s += "<td id=\"ddoc-pgr-pct2\"  style='border-style: solid solid solid none;'></td>";
  }
  s += "<td></td>";
  s += "<td id=\"ddoc-pgr-proc\"  style='text-align: center;'></td>";
  s += "<td id=\"ddoc-pgr-speed\" style='text-align: center;'></td>";
  s += "<td id=\"ddoc-pgr-eta\"   style='text-align: center;'></td>";
  s += "</tr>";
  // spacer
  s += "<tr><td colspan=8>&nbsp;</td></tr>";
  // check description
  s += "<tr><td id=\"ddoc-pgr-desc\" colspan=8></td></tr>";
  s += "</table>";
  s += "</td><tr></table>";
  s += "</div>";
  document.write(s);

  ddoc_pgr_setdesc("...");
  ddoc_pgr_update();

  // fire updater 2 times per second
  ddoc_pgr_timeoutid = setInterval("ddoc_pgr_updater()", 500);
}


/**
 * update the progress bar
 */
function ddoc_pgr_update() {
  // speed
  speed = ddoc_pgr_totaltime ? (1000*ddoc_pgr_processed/ddoc_pgr_totaltime) : -1.0;

  if (ddoc_pgr_totalsize > 0) {
    // percent done
    pct = Math.ceil(100 * ddoc_pgr_processed / ddoc_pgr_totalsize);

    // compute spent time
    nowtime   = (new Date()).getTime();
    ddoc_pgr_totaltime = nowtime - ddoc_pgr_starttime;

    // estimated time
    eta   = speed < 0 ? -1.0 : Math.ceil((ddoc_pgr_totalsize - ddoc_pgr_processed) / speed);

    //
    pctsize = ddoc_pgr_barsize * pct / 100;
    document.getElementById("ddoc-pgr-pct"  ).innerHTML = pct + "%&nbsp;";
    document.getElementById("ddoc-pgr-pct1" ).style.width = pctsize;
    document.getElementById("ddoc-pgr-pct2" ).style.width = ddoc_pgr_barsize-pctsize;
    document.getElementById("ddoc-pgr-eta"  ).innerHTML = ddoc_sec_to_timestr(eta);
  } else {
    pos0 = (2 * ddoc_pgr_ticks) % (ddoc_pgr_barsize * 2 - ddoc_pgr_tickersize);
    pos2 = ddoc_pgr_barsize-ddoc_pgr_tickersize - pos0;
    document.getElementById("ddoc-pgr-pct0" ).style.width = pos0;
    document.getElementById("ddoc-pgr-pct2" ).style.width = pos2;
    document.getElementById("ddoc-pgr-eta"  ).innerHTML = ddoc_pgr_l_na;
  }

  document.getElementById("ddoc-pgr-proc" ).innerHTML = ddoc_pgr_processed;
  document.getElementById("ddoc-pgr-speed").innerHTML = ddoc_speed_tostr(speed);
}


/**
 *
 */
function ddoc_pgr_setdesc(desc) {
  document.getElementById("ddoc-pgr-desc" ).innerHTML = desc;
}


/**
 *
 */
function ddoc_pgr_updater() {
  // don't tick if nothing has changed
  if (ddoc_pgr_processed != ddoc_pgr_last_processed) {
    ddoc_pgr_ticks += 1;
    ddoc_pgr_last_processed = ddoc_pgr_processed;
  }

  // update
  ddoc_pgr_update();
}


/**
 * process element in progress bar
 */
function ddoc_pgr_process(desc) {
  ddoc_pgr_processed += 1;   // one more
  ddoc_pgr_setdesc(desc);    // set description
}



/**
 * finish progress bar
 */
function ddoc_pgr_finish() {
  // remove timeout handler for updater
  clearTimeout(ddoc_pgr_timeoutid);

  // remove progress bar
  if (! ddoc_pgr_quiet) {
    ddoc_element_off("ddoc-pgr-title");
    ddoc_element_clear_id("ddoc-pgr-title"   );
  }
  ddoc_element_off("ddoc-pgr-pbar" );
  ddoc_element_clear_id("ddoc-pgr-pbar"    );
  ddoc_element_clear_id("ddoc-pgr-pbar-out");
  ddoc_element_clear_id("ddoc-pgr-pbar-in" );
  ddoc_element_clear_id("ddoc-pgr-pct"     );
  ddoc_element_clear_id("ddoc-pgr-pct0"    );
  ddoc_element_clear_id("ddoc-pgr-pct1"    );
  ddoc_element_clear_id("ddoc-pgr-pct2"    );
  ddoc_element_clear_id("ddoc-pgr-proc"    );
  ddoc_element_clear_id("ddoc-pgr-speed"   );
  ddoc_element_clear_id("ddoc-pgr-eta"     );
  ddoc_element_clear_id("ddoc-pgr-desc"    );
}
