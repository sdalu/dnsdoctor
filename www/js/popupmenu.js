// $Id: popupmenu.js,v 1.1.1.1 2004-08-11 18:51:04 sdalu Exp $

// 
// CONTACT     : http://www.dnsdoctor.org/
// AUTHOR      : Stephane D'Alu <sdalu@nic.fr>
//
// CREATED     : 2003/02/18 14:33:14
// REVISION    : $Revision: 1.1.1.1 $ 
// DATE        : $Date: 2004-08-11 18:51:04 $
//
// CONTRIBUTORS: (see also CREDITS file)
//
//
// LICENSE     : GPL v2
// COPYRIGHT   : AFNIC (c) 2003
//
// This file is part of DNSdoctor.
//
// DNSdoctor is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// DNSdoctor is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DNSdoctor; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//

// sanity check
if (ddoc_publish_path == null)
  alert("DNSdoctor internal error: ddoc_publish_path not initialized");


/* 
 * DDOC_Popup
 */
function DDOC_Popup(id, title) {
    this.id    = id;
    this.title = title
    this.menu  = null;
    this.item  = [];
}

DDOC_Popup.prototype.create = function() {
  var row, cell, link;
  var self  = this
  var table = document.createElement('TABLE');
  var tbody = document.createElement('TBODY');
  table.appendChild(tbody);
  table.cellSpacing = 1;
  table.className   = "ddoc-popup";
  table.id          = this.id;  // XXX: not working
  table.style.visibility = 'hidden';
  table.style.position   = "absolute";

  // title bar
  row              = document.createElement('TR');
  row.className    = 'ddoc-title';


  cell             = document.createElement('TD');
  cell.className   = 'ddoc-title';
  cell.colSpan     = 7;
  if (this.title) 
    cell.innerHTML = this.title;
  row.appendChild(cell);

  cell             = document.createElement('TD');
  cell.colSpan     = 1;
  cell.align       = 'right';
  link             = document.createElement('A');
  link.href        = '#';
  link.onclick     = function() { self.hide();             return false; };
  link.onmouseover = function() { window.status = "close"; return true;  };
  link.onmouseout  = function() { window.status = "";      return true;  };

  var x = document.createTextNode('x');
  link.appendChild(x);
  cell.appendChild(link);
  row.appendChild(cell);
  tbody.appendChild(row);
  
  // items
  for (var i = 0 ; i < this.item.length ; i++) {
    row            = document.createElement('TR');
    row.className  = "ddoc-item";
    cell           = document.createElement('TD');
    cell.colSpan   = 8;
    cell.align     = 'left';
    cell.innerHTML = this.item[i][0];
    cell.onclick   = this.item[i][1];
    row.appendChild(cell);
    tbody.appendChild(row);
  }
  
  document["body"].appendChild(table);
  this.menu = table;
}

DDOC_Popup.prototype.hide   = function() {
  this.menu.style.visibility = 'hidden';
}

DDOC_Popup.prototype.show   = function(x, y) {
  var left = document.body.scrollLeft;
  var top  = document.body.scrollTop;

  if (this.menu.offsetWidth + x > document.body.clientWidth) {
    left += document.body.clientWidth - this.menu.offsetWidth;
  } else {
    left += x;
  }

  if (this.menu.offsetHeight + y > document.body.clientHeight) {
    top += document.body.clientHeight - this.menu.offsetHeight;
  } else {
    top += y;
  }

  this.menu.style.left     = left + "px";
  this.menu.style.top      = top  + "px";
  this.menu.style.visibility = 'visible';
}

DDOC_Popup.prototype.add    = function(label, func) {
  this.item.push([label, func])
}


/***********************************************************************/

function ddoc_contextmenu_setlocale(l10n_testname,   l10n_details,
				  l10n_references, l10n_elements) {
  ddoc_l10n_testname   = l10n_testname;
  ddoc_l10n_details    = l10n_details;
  ddoc_l10n_references = l10n_references;
  ddoc_l10n_elements   = l10n_elements;
}

function ddoc_contextmenu_start() {
  var hidefunc = function (className, tagName) {
    var elt = document.getElementsByTagName(tagName);
    for (var i = 0 ; i < elt.length ; i++) {
      if (elt[i].className == className) {
	if (elt[i].style.display == "none") {
	  elt[i].style.display     = elt[i].style.display_old;
	} else {
	  elt[i].style.display_old = elt[i].style.display;
	  elt[i].style.display     = "none";
	}
      }
    }
  };

  var ctx = new DDOC_Popup("ddoc_contextmenu", "+/-");
  ctx.add("<IMG src='"+ ddoc_publish_path+"/img/gear.png'>"+
	  "&nbsp;"+ddoc_l10n_testname,
	  function () { hidefunc('ddoc-name', 'DIV'); });
  ctx.add("<IMG src='"+ ddoc_publish_path+"/img/details.png'>"+
	  "&nbsp;"+ddoc_l10n_details,
	  function () { hidefunc('ddoc-details', 'UL'); });
  ctx.add("<IMG src='"+ ddoc_publish_path+"/img/ref.png'>"+
	  "&nbsp;"   +ddoc_l10n_references,
	  function () { hidefunc('ddoc-ref', 'UL'); });
  ctx.add("<IMG src='"+ ddoc_publish_path+"/img/element.png'>"+
	  "&nbsp;"+ddoc_l10n_elements,
	  function () { hidefunc('ddoc-element', 'UL'); });

  ctx.create();

  document.oncontextmenu = function(event) { 
    if (event == null)       // fucking IE
      event = window.event;  //  ok, it hasn't been standardize
    ctx.show(event.clientX, event.clientY); 
    return false; 
  };
}
