#!/bin/sh

warn() { echo "WARN: $1"  ; return 1; }
die()  { echo "ERROR: $1" ; exit   1; }
info() { echo "$1"        ; return 1; }

# Arguments
[ -z "$1" ] && die "version requiered (ex: 2.0.0)"

dest=${2:-/dev/null}
[ "${dest#/}" != ${dest} ] || dest=`pwd`/$dest


release=$1
tmp=/tmp/zcrelease.$$

cvstag=DDOC-`echo $release | sed 's/\./_/g'`
module=dnsdoctor
tarname=$module-$release.tgz
tarlatest=$module-latest.tgz
web=http://www.dnsdoctor.org/

info "Making DNSdoctor release $release"

info "- setting CVSROOT"
if [ -z "$CVSROOT" ]; then
    if [ -f CVS/Root ]; then
	export CVSROOT=`cat CVS/Root`
    else
	die "unable to guess CVSROOT, you need to set it"
    fi
fi

info "- creating temporary directory $tmp"
mkdir -p $tmp
cd $tmp || die "unable to change directory to $tmp"

info "- exporting from CVS with tag $cvstag"
cvs -q export -r $cvstag $module ||
    die "unable to export release tagged $cvstag"

info "- creating RPM spec"
sed s/@VERSION@/$release/ < $module/contrib/distrib/rpm/dnsdoctor.spec.in > $module/contrib/distrib/rpm/dnsdoctor.spec

info "- fetching documentation"
mkdir -p $module/doc 
(cd $module/doc && wget -nd -k -p -E $web/User_guide $web/Developer_guide $web/FAQ $web/Installation $web/Tests_description )

info "- creating tarball: $tarname"
tar cfz $tarname $module

info "- copy on ${dest}"
cp $tarname ${dest}

#info "- copy on savannah"
#ln -s $tarname $tarlatest
#rsync -e ssh -av $tarname $tarlatest subversions.gnu.org:/upload/dnsdoctor/src

info "- cleaning"
rm -Rf $tmp

exit 0
