#!/usr/local/bin/ruby

DDOC_INSTALL_PATH		= (ENV["DDOC_INSTALL_PATH"] || (ENV["HOME"] || "/homes/sdalu") + "/Repository/dnsdoctor").dup.untaint

DDOC_LIB			= "#{DDOC_INSTALL_PATH}/lib"

$LOAD_PATH << DDOC_LIB

require 'nresolv'

resolver = NResolv::DNS::Client::UDP::new(NResolv::DNS::Config::new("ns1.nic.fr"))


name = NResolv::DNS::Name::create("fr.")
puts name

#resolver.each_resource(name, NResolv::DNS::Resource::IN::AXFR, false) { |r,t,n,rpl|
#    puts "#{r.to_dig}"
#}

resolver.each_resource(name, NResolv::DNS::Resource::IN::NS, false) { |r,t,n,rpl|
    puts "#{r.to_dig}"
}

