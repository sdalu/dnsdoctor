# $Id: installer.rb,v 1.4 2004-09-08 12:14:43 sdalu Exp $

# 
# CONTACT     : http://www.dnsdoctor.org/
# AUTHOR      : Stephane D'Alu <sdalu@sdalu.com>
#
# CREATED     : 2003/10/23 21:04:09
# REVISION    : $Revision: 1.4 $ 
# DATE        : $Date: 2004-09-08 12:14:43 $
#


require 'fileutils'
include FileUtils

# Exit codes
EXIT_ERROR = 1
EXIT_OK    = 0

# Installer
class Installer
    # Establish release version number
    #  XXX: snapshot time is completly wrong
    CVS_NAME	= %q$Name: not supported by cvs2svn $
    VERSION	= (Proc::new { 
		       n = CVS_NAME.split[1]
		       n = /^DDOC-(.*)/.match(n) unless n.nil?
		       n = n[1]                  unless n.nil?
		       n = n.gsub(/_/, '.')      unless n.nil?
		       
		       n || Time::now.strftime("snapshot-%Y%d%m")
		   }).call

    #
    CONFIGURE_FILES = [ 
	'www/dnsdoctor.conf.in', 
	'contrib/distrib/rpm/dnsdoctor.spec.in' ]

    def initialize
	# Try to establish what was the interpreter (ie: ruby) used 
	#  to process this file.
	# If we got it wrong user must define the RUBY variable
	#  (full path is required for correct behaviour)
	interpreter = ENV['SUDO_COMMAND'] || ENV['_']
	if RUBY_PLATFORM =~ /mswin32/ 
	    require 'Win32API'
	    getcli = Win32API::new('kernel32', "GetCommandLine", [], 'P')
	    getcli.call() =~ /^\"([^\"]+)\"/
	    interpreter = $1
	end

	# Deal with parameter as variable environment that must be
	#  defined or undefined
	ARGV.delete_if { |arg|
	    case arg
	    when /^-D(\w+)(?:=(.*))?$/ then ENV[$1] = $2   ; true
	    when /^-U(\w+)$/           then ENV.delete($1) ; true
	    else false
	    end
	}

	# Try to provide sensible default values
	ENV['RUBY'      ] ||= interpreter || 'ruby'
	ENV['PREFIX'    ] ||= '/usr/local'
	ENV['PROGNAME'  ] ||= 'dnsdoctor'
	ENV['HTML_PATH' ] ||= "/#{ENV['PROGNAME']}"
	ENV['ETCDIST'   ] ||= ''
	ENV['CHROOT'    ] ||= ''

	ENV['LIBEXEC'   ] ||= "#{ENV['PREFIX']}/libexec"
	ENV['BINDIR'    ] ||= "#{ENV['PREFIX']}/bin"
	ENV['MANDIR'    ] ||= "#{ENV['PREFIX']}/man"
	ENV['DOCDIR'    ] ||= "#{ENV['PREFIX']}/share/doc"
	ENV['ETCDIR'    ] ||= "#{ENV['PREFIX']}/etc"
	ENV['CGIDIR'    ] ||= "#{ENV['LIBEXEC']}/#{ENV['PROGNAME']}/cgi-bin"
        ENV['WWWDIR'    ] ||= "#{ENV['LIBEXEC']}/#{ENV['PROGNAME']}/www"

	ENV['VERSION'	] ||= VERSION

	# Create some shortcut for later
	@installdir    = "#{ENV['LIBEXEC']}/#{ENV['PROGNAME']}"
	@confdir       = "#{ENV['ETCDIR']}/#{ENV['PROGNAME']}#{ENV['ETCDIST']}"
	@dnsdoctor     = "#{@installdir}/dnsdoctor/zc.rb"
	@wwwdir        = "#{ENV['WWWDIR']}"

	@ch_installdir = "#{ENV['CHROOT']}#{@installdir}"
	@ch_confdir    = "#{ENV['CHROOT']}#{@confdir}"
	@ch_dnsdoctor  = "#{ENV['CHROOT']}#{@dnsdoctor}"
	@ch_wwwdir     = "#{ENV['CHROOT']}#{@wwwdir}"

	# Be verbose
	@verbose       = true
    end


    def check_configure
	CONFIGURE_FILES.each { |in_filename|
	    out_filename = in_filename.gsub(/\.in$/, "")
	    if !File.exist?(out_filename)
		$stderr.puts "ERROR: You need to apply the 'configure' rule first."
		exit EXIT_ERROR
	    end
	}
    end

    def configure
	puts "==> Configure"
	CONFIGURE_FILES.each { |in_filename|
	    out_filename = in_filename.gsub(/\.in$/, "")
	    puts "Generating: #{out_filename}"
	    content = File.readlines(in_filename)
	    ENV.each { |k, v| 
		content.each { |line| line.gsub!(/@#{k}@/, v) } 
	    }
	    File::open(out_filename, "w") { |io| io.puts content } 
	}
	puts
    end



    def configinfo
	puts "Default values are:"
	[ 'RUBY', 'PREFIX', 'PROGNAME', 'HTML_PATH' ].each { |k|
	    puts "  #{k}=#{ENV[k]}" }
    end



    def inst_doc
	puts "==> Installing documentation"
	mkdir_p "#{ENV['CHROOT']}#{ENV['DOCDIR']}/#{ENV['PROGNAME']}",
	    						:verbose => @verbose
	install ['README', 'HISTORY', 'CREDITS'], 
	    "#{ENV['CHROOT']}#{ENV['DOCDIR']}/#{ENV['PROGNAME']}",
	    :mode => 0644,				:verbose => @verbose
	puts
    end


    def patch_common
	puts "==> Patching core components"
	dnsdoctor_content = File.readlines(@ch_dnsdoctor)
	[   [ /^\#!.*ruby/, "#!#{ENV['RUBY']}" ],
	    [ 'DDOC_INSTALL_PATH', "\\1\"#{@installdir}\"" ],
	    [ 'DDOC_CONFIG_DIR', "\\1\"#{ENV['ETCDIR']}/#{ENV['PROGNAME']}\""],
	    [ 'DDOC_LOCALIZATION_DIR', "\\1\"#{@installdir}/locale\"" ],
	    [ 'DDOC_TEST_DIR',  "\\1\"#{@installdir}/test\"" ],
	    [ 'DDOC_HTML_PATH', "\\1\"#{ENV['HTML_PATH']}\"" ] ].each { |pattern, value|
	    dnsdoctor_content.each { |line|
		case pattern
		when Regexp
		    line.gsub!(pattern, value)
		when String
		    line.gsub!(/^(#{pattern}\s*=\s*(?:ENV\[[^\]]+\]\s*\|\|\s*)?).*/, value)
		end
	    }
	}
	File::open(@ch_dnsdoctor, "w") { |io| io.puts dnsdoctor_content } 
	chmod 0755, @ch_dnsdoctor,			:verbose => @verbose
	puts
    end

    def inst_common
	puts "==> Installing core components"
	mkdir_p	@ch_installdir,				:verbose => @verbose
	cp_r	"dnsdoctor", @ch_installdir,		:verbose => @verbose
	chmod 0755, @ch_dnsdoctor,			:verbose => @verbose
	puts

	puts "==> Installing libraries"
	cp_r 'lib', @ch_installdir,			:verbose => @verbose
	puts

	puts "==> Installing tests"
	cp_r 'test', @ch_installdir,			:verbose => @verbose
	puts

	puts "==> Installing locale"
	cp_r 'locale', @ch_installdir,			:verbose => @verbose
	puts

	puts "==> Installing default configuration file"
	mkdir_p @ch_confdir,				:verbose => @verbose
	cp 'etc/dnsdoctor/dnsdoctor.conf', @ch_confdir,	:verbose => @verbose
	cp 'etc/dnsdoctor/rootservers',    @ch_confdir,	:verbose => @verbose
	cp Dir['etc/dnsdoctor/*.profile'], @ch_confdir,	:verbose => @verbose
	puts
    end


    def inst_cli
	puts "==> Installing CLI"
	mkdir_p "#{ENV['CHROOT']}#{ENV['BINDIR']}",	:verbose => @verbose
	ln_s @dnsdoctor, "#{ENV['CHROOT']}#{ENV['BINDIR']}/#{ENV['PROGNAME']}",
	    :force => true,				:verbose => @verbose
	mkdir_p "#{ENV['CHROOT']}#{ENV['MANDIR']}/man1",:verbose => @verbose
	install "man/dnsdoctor.1",
	    "#{ENV['CHROOT']}#{ENV['MANDIR']}/man1/#{ENV['PROGNAME']}.1",
	    :mode => 0644,				:verbose => @verbose
	puts
    end


    def patch_cgi
	puts "==> Patching HTML pages"
	Dir["#{@ch_wwwdir}/html/*.html.*"].each { |page|
	    page_content = File.readlines(page)
	    page_content.each { |line| 
		line.gsub!(/@HTML_PATH@/, ENV['HTML_PATH']) }
	    File::open(page, "w", 0644) { |io| io.puts page_content }
	}
	puts
    end

    def inst_cgi
	puts "==> Installing HTML pages"
	mkdir_p @ch_wwwdir,				:verbose => @verbose
	Dir["www/*"].each { |entry|
	    cp_r entry, @ch_wwwdir,			:verbose => @verbose
	}
	puts

	puts "==> Installing CGI"
	mkdir_p "#{ENV['CHROOT']}#{ENV['CGIDIR']}",	:verbose => @verbose
	ln_s @dnsdoctor, "#{ENV['CHROOT']}#{ENV['CGIDIR']}/diagnose.cgi",
	    :force => true,				:verbose => @verbose
	puts
    end



    def info
	puts "==> Info"
	unless ENV['ETCDIST'].empty?
print <<EOT
- DNSdoctor configuration files have been installed in a distribution
   specific directory, so that you can merge them with your current
   configuration.
  If it is the first time you install DNSdoctor you can simply do:
   mv #{@confdir} #{ENV['ETCDIR']}/#{ENV['PROGNAME']}
EOT
	end
	puts ""
        puts "!!! Before using DNSdoctor, please edit the file: dnsdoctor.conf"
	puts ""
    end



    #-- [RULES] -----------------------------------------------------------

    def rule_all
	configure
	inst_common ; patch_common 
	inst_cli
	inst_cgi    ; patch_cgi
	inst_doc
	true
    end
    def rule_cli
	check_configure
	inst_cli
	true
    end
    def rule_common
	check_configure
	inst_common ; patch_common
	true
    end
    def rule_cgi
	check_configure
	inst_cgi    ; patch_cgi
	true
    end
    def rule_doc
	check_configure
	inst_doc
	true
    end
    def rule_configinfo
	configinfo
	false
    end
    def rule_configure
	configure
	false
    end
    def rule_help
	puts "Syntax is:"
	puts "  ruby ./installer.rb [-Dvar=value] [-Uvar] rules..."
	puts
	configinfo
	puts
	puts "Common rules are :"
	puts "  configure: configure DNSdoctor for your system"
	puts "  common   : install files that are necessary for cli and cgi"
	puts "  cli      : install the cli part"
	puts "  cgi      : install the cgi part"
	puts "  doc      : install the documentation"
	puts "  all      : perform all the actions listed above"
	puts "  help     : this description"
	puts
	puts "Recommand rule is: all"
	puts 
	puts "Example:"
	puts "  ruby ./installer.rb -DPREFIX=/opt all"
	puts
    end
end 


#----------------------------------------------------------------------

#
# Instantiate installer
#
inst = Installer::new
info = false


#
# Sanity check
#
rubyver = `#{ENV['RUBY']} --version`
m = /^ruby\s+(\d+)\.(\d+)\.(\d+)\s+/.match(rubyver)
if m.nil?
    $stderr.puts "ERROR: Invalid interpreter: #{ENV['RUBY']}"
    exit EXIT_ERROR
end

if (m[1].to_i <= 1) && (m[2].to_i < 8)
    $stderr.puts "WARNING: ruby version 1.8.0 at least is required"
    $stderr.puts "WARNING: Hopping that the one defined in RUBY is more recent"
end


#
#
#
if ARGV.empty?
    inst.rule_help
else
    ARGV.each { |rule|
	unless inst.respond_to?("rule_#{rule}")
	    puts "ERROR: No rule '#{rule}' available"
	    exit 1
	end
    }

    ARGV.each { |rule|
	info |= inst.send "rule_#{rule}" 
    }
    
    inst.info if info
end

exit EXIT_OK
