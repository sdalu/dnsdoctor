# $Id: dns.rb,v 1.1.1.1 2004-08-11 18:51:04 sdalu Exp $

# 
# AUTHOR   : Stephane D'Alu <sdalu@nic.fr>
# CREATED  : 2002/08/02 13:58:17
#
# COPYRIGHT: AFNIC (c) 2003
# CONTACT  : 
# LICENSE  : RUBY
#
# $Revision: 1.1.1.1 $ 
# $Date: 2004-08-11 18:51:04 $
#
# INSPIRED BY:
#   - the ruby file: resolv.rb 
#
# CONTRIBUTORS: (see also CREDITS file)
#
#

require 'address'

class NResolv
    class NResolvError < StandardError
    end
end

load 'nresolv/dns_name.rb'
load 'nresolv/dns_resource.rb'
load 'nresolv/dns_message.rb'

