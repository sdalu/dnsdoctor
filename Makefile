# $Id: Makefile,v 1.2 2004-09-07 08:51:02 sdalu Exp $

#  
# CONTACT     : http://www.dnsdoctor.org/ 
# AUTHOR      : Stephane D'Alu <sdalu@sdalu.com>
# 
# CREATED     : 2003/10/23 21:04:09 
# REVISION    : $Revision: 1.2 $  
# DATE        : $Date: 2004-09-07 08:51:02 $ 
# 

RUBY ?=ruby
ZC_INSTALLER=$(RUBY) ./installer.rb -DRUBY="$(RUBY)"


all: configinfo

warning:
	@echo "Normally you should use:"
	@echo "  ruby ./installer.rb"
	@echo
	@echo "But if you are used to use $(MAKE), you can perform a full"
	@echo "installation by using the target: install"
	@echo

configinfo: warning
	@$(ZC_INSTALLER) configinfo
	@echo ""
	@echo "You can change them by using the syntax:"
	@echo "  $(MAKE) key=value"

install: warning
	@$(ZC_INSTALLER) all
