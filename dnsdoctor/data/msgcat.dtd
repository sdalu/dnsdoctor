<!--                                                                      -->
<!-- CONTACT     : http://www.dnsdoctor.org/                              -->
<!-- AUTHOR      : Stephane D'Alu <sdalu@sdalu.com>                       -->
<!--                                                                      -->
<!-- CREATED     : 2003/06/28 00:09:51                                    -->
<!-- REVISION    : $Revision: 1.1.1.1 $                                      -->
<!-- DATE        : $Date: 2004-08-11 18:51:04 $                           -->
<!--                                                                      -->
<!-- CONTRIBUTORS: (see also CREDITS file)                                -->
<!--                                                                      -->
<!--                                                                      -->
<!-- LICENSE     : GPL v2                                                 -->
<!-- COPYRIGHT   : Stephane D'Alu (c) 2003                                -->
<!--                                                                      -->
<!-- This file is part of DNSdoctor.                                      -->
<!--                                                                      -->
<!-- DNSdoctor is free software; you can redistribute it and/or modify it -->
<!-- under the terms of the GNU General Public License as published by    -->
<!-- the Free Software Foundation; either version 2 of the License, or    -->
<!-- (at your option) any later version.                                  -->
<!--                                                                      -->
<!-- DNSdoctor is distributed in the hope that it will be useful, but     -->
<!-- WITHOUT ANY WARRANTY; without even the implied warranty of           -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU    -->
<!-- General Public License for more details.                             -->
<!--                                                                      -->
<!-- You should have received a copy of the GNU General Public License    -->
<!-- along with DNSdoctor; if not, write to the Free Software Foundation, -->
<!-- Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA          -->
<!--                                                                      -->


<!ELEMENT msgcat  ((shortcut|check|test)*|(section|tag)*)>

<!ELEMENT shortcut (explanation|details)*>

<!ELEMENT check    (name,success,failure,explanation,details)>
<!ELEMENT test     (name)>

<!ELEMENT name		(#PCDATA|var|const)*>
<!ELEMENT failure	(#PCDATA|var|const)*>
<!ELEMENT success	(#PCDATA|var|const)*>
<!ELEMENT explanation	(src*)>
<!ELEMENT details	(para*)>

<!ELEMENT src           (title,para+)>
<!ELEMENT title		(#PCDATA|var|const)*>
<!ELEMENT para          (#PCDATA|var|const|uri)*>

<!ELEMENT var		EMPTY>
<!ELEMENT const		EMPTY>
<!ELEMENT uri		(#PCDATA)>
<!ELEMENT tag           (#PCDATA)>
<!ELEMENT section       (tag*)>

<!ATTLIST msgcat       lang    CDATA #REQUIRED>

<!ATTLIST check        name    CDATA #REQUIRED>
<!ATTLIST test         name    CDATA #REQUIRED>

<!ATTLIST explanation  name    CDATA #IMPLIED
                       sameas  CDATA #IMPLIED>
<!ATTLIST details      name    CDATA #IMPLIED
                       sameas  CDATA #IMPLIED>

<!ATTLIST src          type    CDATA #REQUIRED
                       from    CDATA #IMPLIED
                       fid     CDATA #IMPLIED>
<!ATTLIST uri          link    CDATA #REQUIRED>
<!ATTLIST var          name    CDATA #REQUIRED>
<!ATTLIST var          display CDATA #IMPLIED>
<!ATTLIST const        name    CDATA #REQUIRED>
<!ATTLIST const        display CDATA #IMPLIED>

<!ATTLIST section      name    CDATA #REQUIRED>
<!ATTLIST tag          name    CDATA #REQUIRED>
