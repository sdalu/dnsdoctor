# $Id: gtk.rb,v 1.1.1.1 2004-08-11 18:51:04 sdalu Exp $

# 
# CONTACT     : http://www.dnsdoctor.org/
# AUTHOR      : Stephane D'Alu <sdalu@nic.fr>
#
# CREATED     : 2003/01/06 15:18:23
# REVISION    : $Revision: 1.1.1.1 $ 
# DATE        : $Date: 2004-08-11 18:51:04 $
#
# CONTRIBUTORS: (see also CREDITS file)
#
#
# LICENSE     : GPL v2
# COPYRIGHT   : AFNIC (c) 2003
#
# This file is part of DNSdoctor.
#
# DNSdoctor is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# DNSdoctor is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with DNSdoctor; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#

module Gtk
    class Label
        def self.from_i18n(tag)
            self.new($mc.get(tag))
        end
    end

    class Frame
        def self.from_i18n(tag)
            self.new($mc.get(tag))
        end
    end

    class Table
        def attach_key_value(idx, key, value, 
                             selectable=false, separator=' : ')
            keylbl      = Gtk::Label::new(key)
            keylbl.set_alignment(0, 0.5)
            vallbl      = Gtk::Label::new(value)
            vallbl.set_alignment(0, 0.5)
            vallbl.set_selectable(selectable)

            xpos = 0
            
            attach(keylbl, xpos, xpos+1, idx, idx+1, Gtk::SHRINK|Gtk::FILL)
            xpos += 1
            if separator
                seplbl  = Gtk::Label::new(separator)
                attach(seplbl, xpos, xpos+1, idx, idx+1, Gtk::SHRINK|Gtk::FILL)
                xpos += 1
            end
            attach(vallbl, xpos, xpos+1, idx, idx+1, Gtk::SHRINK|Gtk::FILL)
            self
        end
    end

    class Button
	alias _initialize initialize
	def initialize(*args)
	    if (args.length > 1)
		lhs = case args[0]
		      when Symbol     then Gtk::Image::new(args[0], 
							 Gtk::IconSize::BUTTON)
		      when Gtk::Image then args[0]
		      else return _initialize(*args) 
		      end
		rhs = case args[1]
		      when String     then Gtk::Label::new(args[1])
		      else return _initialize(*args) 
		      end

		hbox  = Gtk::HBox::new(false)
		hbox.pack_start(lhs, false, false, 2)
		hbox.pack_start(rhs, false, false, 0)
		align = Gtk::Alignment::new(0.5, 0.5, 0, 0)
		align.child = hbox
		_initialize()
		self.child = align
		return self
	    else
		return  _initialize(*args)
	    end
	end
    end
end
